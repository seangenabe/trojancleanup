
var fs = require('fs');
var dive = require('dive');
var path = require('path');
var Q = require('q');

var regex = /(\<SCRIPT\sLanguage\=VBScript\>[\S\s]*)/g

var p = Q();

var enqueueFile = function(file) {
    p = p.then(function() {
        return processFile(file)
    });
}

var stat = Q.denodeify(fs.stat);
var readFile = Q.denodeify(fs.readFile);
var writeFile = Q.denodeify(fs.writeFile);

var processFile = function(file) {
    if (file) {
        return stat(file).then(function(stat) {
            var extname = path.extname(file)
            if (stat.isFile() && (extname == '.html' || extname == '.htm')) {
                return readFile(file).then(function(data) {
                    data = data.toString();
                    var match = regex.exec(data);
                    if (match && match[1]) {
                        console.log('Infected: ' + file);
                        data = data.replace(regex, '');
                        return writeFile(file, data).catch(function(reason) {
                            console.log('Unable to write to file: ' + file);
                        });
                    }
                });
            }
        })
    }
}

dive(process.cwd(), {all: true, files: true}, function(err, file) {
    enqueueFile(file);
});
